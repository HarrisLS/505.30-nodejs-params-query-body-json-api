//Import thư viện expressjs tương đương import express from "express";
const express = require("express");

//Khởi tạo 1 app express
const app = express();

// khai báo cổng chạy project
const port = 8000;

//Khao báo để app đọc dc Body Json
app.use(express.json());

// Callback function là 1 function đóng vai trò là tham số của 1 func khác, nó sẽ dc thực hiện khi func chủ dc gọi
// Khai báo API dạng /
app.get("/", (req, res) => {
  let today = new Date();

  res.json({
    message: `Xin chào, hôm nay là ngày ${today.getDate()} tháng ${
      today.getMonth() + 1
    } năm ${today.getFullYear()}`,
  });
});

// Khai báo API dạng GET
app.get("/get-method", (req, res) => {
  res.json({
    message: "GET method",
  });
});

// Khai báo dạng POST
app.post("/post-method", (req, res) => {
  res.json({
    message: "POST method",
  });
});

// Khai báo dạng PUT
app.put("/put-method", (req, res) => {
  res.json({
    message: "PUT method",
  });
});

// Khai báo dạng DELETE
app.delete("/delete-method", (req, res) => {
  res.json({
    message: "DELETE method",
  });
});

// Request params (GET, PUT, DELETE)
// Request params là những tham số xuất hiện tại URL của API
// VD: https://docs.google.com/presentation/d/108ppjKp_ac0ZHNfrNdOyxUPAG9E7Ehf_nKOFs-NniU8/edit#slide=id.g1169ac78037_0_113
app.get("/request-params/:param1/:param2/param", (req, res) => {
  let param1 = req.params.param1;
  let param2 = req.params.param2;

  res.json({
    params: {
      param1,
      param2,
    },
  });
});

// Request Query (chỉ áp dụng cho phương thức GET)
// Khi lấy request-query bắt buộc phải valide
app.get("/request-query", (req, res) => {
  let query = req.query;
  res.json({
    query,
  });
});

//Request Body JSON (chỉ áp dụng cho phương thức POST & PUT)
// Khi lấy request-body-json bắt buộc phải valide
// Cần khai báo thêm ở dòng số 11
app.post("/request-body-json", (req, res) => {
  let body = req.body;

  res.json({
    body,
  });
});

app.listen(port, () => {
  console.log("App listening on port: ", port);
});
